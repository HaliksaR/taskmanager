package ru.haliksar.taskmanager.models

data class CpuInfo(
    var name: String,
    var vendor_id: String,
    var architecture: String,
    var Threads_per_core: Int,
    var Cores_per_socket: Int,
    var CPU_max_MHz: Double,
    var CPU_min_MHz: Double,
    var cache: Array<Double>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CpuInfo

        if (name != other.name) return false
        if (vendor_id != other.vendor_id) return false
        if (architecture != other.architecture) return false
        if (Threads_per_core != other.Threads_per_core) return false
        if (Cores_per_socket != other.Cores_per_socket) return false
        if (CPU_max_MHz != other.CPU_max_MHz) return false
        if (CPU_min_MHz != other.CPU_min_MHz) return false
        if (!cache.contentEquals(other.cache)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + vendor_id.hashCode()
        result = 31 * result + architecture.hashCode()
        result = 31 * result + Threads_per_core
        result = 31 * result + Cores_per_socket
        result = 31 * result + CPU_max_MHz.hashCode()
        result = 31 * result + CPU_min_MHz.hashCode()
        result = 31 * result + cache.contentHashCode()
        return result
    }
}
