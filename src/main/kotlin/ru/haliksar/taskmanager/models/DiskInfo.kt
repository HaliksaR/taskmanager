package ru.haliksar.taskmanager.models

import javafx.beans.property.SimpleLongProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.getValue
import tornadofx.setValue

/**
 * @param name device name
 * @param r_completed reads completed successfully
 * @param r_merged reads merged
 * @param r_sectors sectors read
 * @param r_time_spent time spent reading (ms)
 * @param w_completed writes completed
 * @param w_merged writes merged
 * @param w_sectors sectors written
 * @param w_time_spent time spent writing (ms)
 * @param IO_currently I/Os currently in progress
 * @param IO_time_spent time spent doing I/Os (ms)
 * @param IO_weighted_time_spent weighted time spent doing I/Os (ms)
 */

class DiskInfo(
    name: String,
    blocks: Long,
    r_completed: Long,
    r_merged: Long,
    r_sectors: Long,
    r_time_spent: Long,
    w_completed: Long,
    w_merged: Long,
    w_sectors: Long,
    w_time_spent: Long,
    IO_currently: Long,
    IO_time_spent: Long,
    IO_weighted_time_spent: Long
) {
    val nameProperty = SimpleStringProperty(this, "name", name)
    var name by nameProperty

    val blocksProperty = SimpleLongProperty(this, "blocks", blocks)
    var blocks by blocksProperty

    val r_completedProperty = SimpleLongProperty(this, "r_completed", r_completed)
    var r_completed by r_completedProperty

    val r_mergedProperty = SimpleLongProperty(this, "r_merged", r_merged)
    var r_merged by r_mergedProperty

    val r_sectorsProperty = SimpleLongProperty(this, "r_sectors", r_sectors)
    var r_sectors by r_sectorsProperty

    val r_time_spentProperty = SimpleLongProperty(this, "r_time_spent", r_time_spent)
    var r_time_spent by r_time_spentProperty

    val w_completedProperty = SimpleLongProperty(this, "w_completed", w_completed)
    var w_completed by w_completedProperty

    val w_mergedProperty = SimpleLongProperty(this, "w_merged", w_merged)
    var w_merged by w_mergedProperty

    val w_sectorsProperty = SimpleLongProperty(this, "w_sectors", w_sectors)
    var w_sectors by w_sectorsProperty

    val w_time_spentProperty = SimpleLongProperty(this, "w_time_spent", w_time_spent)
    var w_time_spent by w_time_spentProperty

    val IO_currentlyProperty = SimpleLongProperty(this, "IO_currently", IO_currently)
    var IO_currently by IO_currentlyProperty

    val IO_time_spentProperty = SimpleLongProperty(this, "IO_time_spent", IO_time_spent)
    var IO_time_spent by IO_time_spentProperty

    val IO_weighted_time_spentProperty = SimpleLongProperty(this, "IO_weighted_time_spent", IO_weighted_time_spent)
    var IO_weighted_time_spent by IO_weighted_time_spentProperty

    override fun toString(): String {
        return "{\n   name: $name,\n" +
                "    blocks: $blocks,\n" +
                "    r_completed: $r_completed,\n" +
                "    r_merged: $r_merged,\n" +
                "    r_sectors: $r_sectors,\n" +
                "    r_time_spent: $r_time_spent,\n" +
                "    w_completed: $w_completed,\n" +
                "    w_merged: $w_merged,\n" +
                "    w_sectors: $w_sectors,\n" +
                "    w_time_spent: $w_time_spent,\n" +
                "    IO_currently: $IO_currently,\n" +
                "    IO_time_spent: $IO_time_spent,\n" +
                "    IO_weighted_time_spent: $IO_weighted_time_spent\n" +
                "}"
    }
}
