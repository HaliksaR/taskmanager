package ru.haliksar.taskmanager.models

import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.getValue
import tornadofx.setValue

class ProcessInfo(
    pid: Int,
    name: String? = null,
    user: String? = null,
    status: String? = null,
    loadCpu: Double,
    loadRam: Double,
    path: String? = null
) {
    val pidProperty = SimpleIntegerProperty(this, "pid", pid)
    var pid by pidProperty

    val nameProperty = SimpleStringProperty(this, "name", name)
    var name by nameProperty

    val userProperty = SimpleStringProperty(this, "user", user)
    var user by userProperty

    val statusProperty = SimpleStringProperty(this, "status", status)
    var status by statusProperty

    val loadCpuProperty = SimpleDoubleProperty(this, "loadCpu", loadCpu)
    var loadCpu by loadCpuProperty

    val loadRamProperty = SimpleDoubleProperty(this, "loadRam", loadRam)
    var loadRam by loadRamProperty

    val pathProperty = SimpleStringProperty(this, "path", path)
    var path by pathProperty
}
