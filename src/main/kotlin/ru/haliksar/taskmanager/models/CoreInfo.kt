package ru.haliksar.taskmanager.models

import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleLongProperty
import tornadofx.getValue
import tornadofx.setValue

class CoreInfo(
    total: Long,
    idle: Long,
    usage: Double
) {
    val totalProperty = SimpleLongProperty(this, "total", total)
    var total by totalProperty

    val idleProperty = SimpleLongProperty(this, "idle", idle)
    var idle by idleProperty

    val usageProperty = SimpleDoubleProperty(this, "usage", usage)
    var usage by usageProperty
}
