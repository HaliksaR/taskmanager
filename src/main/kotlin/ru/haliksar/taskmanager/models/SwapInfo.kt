package ru.haliksar.taskmanager.models

import javafx.beans.property.SimpleDoubleProperty
import tornadofx.getValue
import tornadofx.setValue

class SwapInfo(
    total: Double,
    free: Double,
    used: Double
) {
    val totalProperty = SimpleDoubleProperty(this, "total", total)
    var total by totalProperty

    val freeProperty = SimpleDoubleProperty(this, "free", free)
    var free by freeProperty

    val usedProperty = SimpleDoubleProperty(this, "used", used)
    var used by usedProperty
}
