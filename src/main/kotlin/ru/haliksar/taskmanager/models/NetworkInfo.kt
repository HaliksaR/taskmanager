package ru.haliksar.taskmanager.models

import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleLongProperty
import tornadofx.getValue
import tornadofx.setValue

class NetworkInfo(
    r_bytes: Long?,
    r_packets: Long?,
    r_speed: Double?,
    t_bytes: Long?,
    t_packets: Long?,
    t_speed: Double?
) {
    val r_bytesProperty = SimpleLongProperty(this, "r_bytes", r_bytes ?: 0L)
    var r_bytes by r_bytesProperty

    val r_packetsProperty = SimpleLongProperty(this, "r_packets", r_packets ?: 0L)
    var r_packets by r_packetsProperty

    val r_speedProperty = SimpleDoubleProperty(this, "r_speed", r_speed ?: 0.0)
    var r_speed by r_speedProperty

    val t_bytesProperty = SimpleLongProperty(this, "t_bytes", t_bytes ?: 0L)
    var t_bytes by t_bytesProperty

    val t_packetsProperty = SimpleLongProperty(this, "t_packets", t_packets ?: 0L)
    var t_packets by t_packetsProperty

    val t_speedProperty = SimpleDoubleProperty(this, "t_speed", t_speed ?: 0.0)
    var t_speed by t_speedProperty
}
