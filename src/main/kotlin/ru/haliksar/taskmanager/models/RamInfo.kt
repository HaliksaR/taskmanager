package ru.haliksar.taskmanager.models

import javafx.beans.property.SimpleDoubleProperty
import tornadofx.getValue
import tornadofx.setValue

class RamInfo(
    total: Double,
    free: Double,
    available: Double,
    used: Double
) {
    val totalProperty = SimpleDoubleProperty(this, "total", total)
    var total by totalProperty

    val freeProperty = SimpleDoubleProperty(this, "free", free)
    var free by freeProperty

    val availableProperty = SimpleDoubleProperty(this, "available", available)
    var available by availableProperty

    val usedProperty = SimpleDoubleProperty(this, "used", used)
    var used by usedProperty
}
