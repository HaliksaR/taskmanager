package ru.haliksar.taskmanager.view

import javafx.application.Platform
import javafx.beans.binding.Bindings
import javafx.beans.property.SimpleStringProperty
import javafx.scene.Parent
import javafx.scene.chart.CategoryAxis
import javafx.scene.chart.NumberAxis
import javafx.scene.chart.XYChart
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.haliksar.taskmanager.utils.CoreUtils
import ru.haliksar.taskmanager.utils.other.Variable
import ru.haliksar.taskmanager.utils.refreshers.RefreshText
import tornadofx.View
import tornadofx.areachart
import tornadofx.borderpane
import tornadofx.center
import tornadofx.fitToParentHeight
import tornadofx.fitToParentWidth
import tornadofx.scrollpane
import tornadofx.tooltip
import tornadofx.vbox
import java.text.SimpleDateFormat
import java.util.Date
import java.util.concurrent.ThreadLocalRandom
import kotlin.collections.ArrayList

class ChartsView : View() {
    override var root: Parent = borderpane()

    init {
        root = borderpane {
            fitToParentHeight()
            fitToParentWidth()
            center {
                scrollpane {
                    isFitToWidth = true
                    minHeight = height
                    prefHeight = height
                    minWidth = width
                    prefWidth = width
                    vbox {
                        fitToParentWidth()

                        val yAxisP = NumberAxis()
                        yAxisP.run {
                            label = "Persent, %"
                            animated = false
                            isAutoRanging = false
                            lowerBound = 0.0
                            upperBound = 100.0
                        }

                        areachart("Processor", CategoryAxis(), yAxisP) {
                            animated = false
                            createSymbols = false
                            xAxis.label = "Time/s"
                            xAxis.animated = false

                            val series = ArrayList<XYChart.Series<String, Number>>()
                            val cores = CoreUtils.getInfo()
                            val dataProcs = ArrayList<XYChart.Series<String, Number>>()
                            cores.forEach { (index, _) ->
                                if (index != 0) {
                                    val dataProc = XYChart.Series<String, Number>()
                                    dataProc.nameProperty().bind(
                                        Bindings.concat(
                                            "CPU",
                                            index - 1,
                                            ": ",
                                            // TODO Not Refresh!!
                                            RefreshText.cores[index + 1]?.usageProperty ?: 0,
                                            "%"
                                        )
                                    )
                                    dataProcs.add(dataProc)
                                    series.add(dataProcs.last())
                                }
                            }
                            series.forEach {
                                data.add(it)
                            }

                            GlobalScope.launch(Dispatchers.IO) {
                                val simpleDateFormat = SimpleDateFormat("HH:mm:ss")
                                while (true) {
                                    series.forEachIndexed { index, it ->
                                        Platform.runLater {
                                            val dataDate = SimpleStringProperty(simpleDateFormat.format(Date()))

                                            val dataProc = XYChart.Data<String, Number>()
                                            dataProc.XValueProperty().bind(dataDate)
                                            dataProc.YValueProperty().bind(RefreshText.cores[index + 1]!!.usageProperty)
                                            it.data.add(dataProc)
                                            if (it.data.size > 20)
                                                it.data.removeAt(0)
                                        }
                                    }
                                    delay(Variable.delayCharts)
                                }
                            }
                        }

                        val yAxisRS = NumberAxis()
                        yAxisRS.run {
                            label = "Size"
                            animated = false
                            isAutoRanging = false
                            lowerBound = 0.0
                            upperBoundProperty().bind(RefreshText.ram.totalProperty)
                        }

                        areachart("Ram and Swap", CategoryAxis(), yAxisRS) {
                            animated = false
                            createSymbols = false
                            xAxis.label = "Time/s"
                            xAxis.animated = false

                            val series = ArrayList<XYChart.Series<String, Number>>()
                            val dataRam = XYChart.Series<String, Number>()

                            dataRam.nameProperty().bind(
                                Bindings.concat(
                                    "Memory: ",
                                    RefreshText.ram.usedProperty.asString("%.2f"),
                                    "Gb/",
                                    RefreshText.ram.totalProperty.asString("%.2f"),
                                    "Gb"
                                )
                            )
                            series.add(dataRam)
                            this.data.add(dataRam)

                            val dataSwap = XYChart.Series<String, Number>()
                            dataSwap.nameProperty().bind(
                                Bindings.concat(
                                    "Swap: ",
                                    RefreshText.swap.usedProperty.asString("%.2f"),
                                    "Gb/",
                                    RefreshText.swap.totalProperty.asString("%.2f"),
                                    "Gb"
                                )
                            )
                            series.add(dataSwap)
                            this.data.add(dataSwap)

                            GlobalScope.launch(Dispatchers.IO) {
                                val simpleDateFormat = SimpleDateFormat("HH:mm:ss")
                                while (true) {
                                    val swap = RefreshText.swap.usedProperty.value
                                    val ram = RefreshText.ram.usedProperty.value
                                    Platform.runLater {
                                        val dataDate = SimpleStringProperty(simpleDateFormat.format(Date()))

                                        val datRam = XYChart.Data<String, Number>()
                                        datRam.XValueProperty().bind(dataDate)
                                        datRam.yValue = ram

                                        val datSwap = XYChart.Data<String, Number>()
                                        datSwap.XValueProperty().bind(dataDate)
                                        datSwap.yValue = swap

                                        series[0].data.add(datRam)
                                        series[1].data.add(datSwap)

                                        if (series[0].data.size > 20)
                                            series[0].data.removeAt(0)
                                        if (series[1].data.size > 20)
                                            series[1].data.removeAt(0)
                                    }
                                    delay(Variable.delayCharts)
                                }
                            }
                            tooltip {
                                textProperty().bind(
                                    Bindings.concat(
                                        "Memory: ",
                                        RefreshText.ram.usedProperty.asString("%.2f"),
                                        "Gb/",
                                        RefreshText.ram.totalProperty.asString("%.2f"),
                                        "Gb\n",
                                        "Swap: ",
                                        RefreshText.swap.usedProperty.asString("%.2f"),
                                        "Gb/",
                                        RefreshText.swap.totalProperty.asString("%.2f"),
                                        "Gb"
                                    )
                                )
                            }
                        }

                        val yAxisNet = NumberAxis()
                        yAxisNet.run {
                            label = "Kb/s"
                            animated = false
                            lowerBound = 0.0
                        }

                        areachart("Network", CategoryAxis(), yAxisNet) {
                            animated = false
                            createSymbols = false

                            xAxis.label = "Time/s"
                            xAxis.animated = false // axis animations are removed

                            val series = ArrayList<XYChart.Series<String, Number>>()

                            val dataR = XYChart.Series<String, Number>()
                            dataR.nameProperty().bind(
                                Bindings.concat(
                                    "Receiving: ",
                                    RefreshText.sumRSpeedProperty.asString("%.2f"),
                                    "Kb/s"
                                )
                            )
                            series.add(dataR)
                            this.data.add(dataR)

                            val dataT = XYChart.Series<String, Number>()
                            dataT.nameProperty().bind(
                                Bindings.concat(
                                    "Transmit: ",
                                    RefreshText.sumTSpeedProperty.asString("%.2f"),
                                    "Kb/s"
                                )
                            )
                            series.add(dataT)
                            this.data.add(dataT)

                            GlobalScope.launch(Dispatchers.IO) {
                                val simpleDateFormat = SimpleDateFormat("HH:mm:ss")
                                while (true) {
                                    val numR = RefreshText.sumRSpeedProperty.value
                                    val numT = RefreshText.sumTSpeedProperty.value
                                    Platform.runLater {
                                        val dataDate = SimpleStringProperty(simpleDateFormat.format(Date()))

                                        val datRam = XYChart.Data<String, Number>()
                                        datRam.XValueProperty().bind(dataDate)
                                        datRam.yValue = numR

                                        val datSwap = XYChart.Data<String, Number>()
                                        datSwap.XValueProperty().bind(dataDate)
                                        datSwap.yValue = numT

                                        series[0].data.add(datRam)
                                        series[1].data.add(datSwap)

                                        if (series[0].data.size > 20)
                                            series[0].data.removeAt(0)
                                        if (series[1].data.size > 20)
                                            series[1].data.removeAt(0)
                                    }
                                    delay(Variable.delayCharts)
                                }
                            }
                        }

                        areachart("Disk", CategoryAxis(), NumberAxis()) {
                            animated = false
                            createSymbols = false

                            xAxis.label = "Time/s"
                            xAxis.animated = false // axis animations are removed
                            yAxis.label = "Value"
                            yAxis.animated = false // axis animations are removed

                            val series = ArrayList<XYChart.Series<String, Number>>()
                            for (i in 0 until 2) {
                                val data = XYChart.Series<String, Number>()
                                data.name = "Data Series $i"
                                series.add(data)
                            }

                            series.forEach {
                                this.data.add(it)
                            }

                            GlobalScope.launch(Dispatchers.IO) {
                                val simpleDateFormat = SimpleDateFormat("HH:mm:ss")
                                while (true) {
                                    series.forEach {
                                        // Update the chart
                                        val random = ThreadLocalRandom.current().nextInt(10)
                                        Platform.runLater {
                                            // get current time
                                            val now = Date()
                                            // put random number with current time
                                            it.data.add(XYChart.Data(simpleDateFormat.format(now), random))
                                            if (it.data.size > 20)
                                                it.data.removeAt(0)
                                        }
                                    }
                                    delay(Variable.delayCharts)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
