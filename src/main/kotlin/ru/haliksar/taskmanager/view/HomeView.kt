package ru.haliksar.taskmanager.view

import javafx.geometry.Pos
import javafx.scene.control.TabPane
import javafx.scene.input.KeyCode
import ru.haliksar.taskmanager.utils.other.Notification
import ru.haliksar.taskmanager.utils.refreshers.RefreshTable
import ru.haliksar.taskmanager.utils.refreshers.RefreshText
import tornadofx.View
import tornadofx.action
import tornadofx.borderpane
import tornadofx.bottom
import tornadofx.box
import tornadofx.c
import tornadofx.center
import tornadofx.hbox
import tornadofx.item
import tornadofx.label
import tornadofx.left
import tornadofx.menu
import tornadofx.menubar
import tornadofx.paddingAll
import tornadofx.px
import tornadofx.right
import tornadofx.style
import tornadofx.tab
import tornadofx.tabpane
import tornadofx.textfield
import tornadofx.top

class HomeView : View("Task Manager") {
    override var root = borderpane()

    init {
        RefreshText.startRealtime()
        RefreshTable.startRealtime()
        markupRoot()
    }

    private fun markupRoot() {
        root = borderpane {
            top {
                borderpane {
                    left {
                        menubar {
                            menu("Table") {
                                menu("Refresh Mode") {
                                    item("Refresh").action {
                                        RefreshTable.startOne()
                                    }
                                    item("Refresh RealTime").action {
                                        RefreshTable.startRealtime()
                                    }
                                }
                            }
                        }
                    }
                    right {
                        textfield() {
                            promptText = " Run program"
                            setOnKeyPressed { event ->
                                if (event.code == KeyCode.ENTER) {
                                    try {
                                        Runtime.getRuntime().exec(text)
                                    } catch (e: Exception) {
                                        Notification.pushWarning(
                                            "Error",
                                            "Program \"$text\" not found"
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
            }
            center {
                tabpane {
                    tabClosingPolicy = TabPane.TabClosingPolicy.UNAVAILABLE
                    tab("Processes") {
                        add(TaskView::class)
                    }
                    tab("Charts") {
                        add(ChartsView::class)
                    }
                }
            }
            bottom {
                borderpane {
                    paddingAll = 5.0
                    style {
                        backgroundColor += c("#cecece")
                        borderColor += box(c("#a1a1a1"))
                        minWidth = 200.px
                    }
                    left {
                        hbox {
                            alignment = Pos.CENTER_LEFT
                            label("Process: ")
                            label() {
                                textProperty().bind(RefreshText.processes.asString())
                            }
                        }
                    }
                    center {
                        hbox {
                            alignment = Pos.CENTER
                            label("CPU: ")
                            label() {
                                textProperty().bind(RefreshText.usageProcessor.asString("%.0f"))
                            }
                            label("%")
                        }
                    }
                    right {
                        hbox {
                            alignment = Pos.CENTER_RIGHT
                            label("RAM: ")
                            label() {
                                textProperty().bind(RefreshText.ram.usedProperty.asString("%.2f"))
                            }
                            label("/")
                            label() {
                                textProperty().bind(RefreshText.ram.totalProperty.asString("%.2f"))
                            }
                        }
                    }
                }
            }
        }
    }
}
