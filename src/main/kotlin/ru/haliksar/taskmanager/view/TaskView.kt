package ru.haliksar.taskmanager.view

import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.scene.control.Button
import javafx.scene.control.ComboBox
import javafx.scene.control.SortEvent
import javafx.scene.control.TableView
import javafx.scene.control.TextField
import javafx.scene.control.skin.NestedTableColumnHeader
import javafx.scene.control.skin.TableHeaderRow
import javafx.scene.control.skin.TableViewSkin
import javafx.scene.input.MouseButton
import javafx.scene.layout.Priority
import javafx.scene.paint.Color
import ru.haliksar.taskmanager.models.ProcessInfo
import ru.haliksar.taskmanager.utils.ProcessUtils
import ru.haliksar.taskmanager.utils.other.Notification
import ru.haliksar.taskmanager.utils.refreshers.RefreshTable
import tornadofx.View
import tornadofx.action
import tornadofx.borderpane
import tornadofx.button
import tornadofx.c
import tornadofx.center
import tornadofx.column
import tornadofx.combobox
import tornadofx.contentWidth
import tornadofx.contextmenu
import tornadofx.hbox
import tornadofx.hboxConstraints
import tornadofx.item
import tornadofx.label
import tornadofx.maxWidth
import tornadofx.minWidth
import tornadofx.onChange
import tornadofx.paddingAll
import tornadofx.paddingBottom
import tornadofx.paddingLeft
import tornadofx.paddingTop
import tornadofx.style
import tornadofx.tableview
import tornadofx.textfield
import tornadofx.top

class TaskView : View() {
    val sort = FXCollections.observableArrayList(
        "All Process",
        "System Process", "User Process"
    )
    val sortProp = SimpleStringProperty()
    lateinit var search: TextField
    lateinit var SelectUser: ComboBox<String>
    lateinit var tableview: TableView<ProcessInfo>
    lateinit var killbtn: Button

    override val root = borderpane {
        minHeight = height
        prefHeight = height
        minWidth = width
        prefWidth = width
        top {
            hbox {
                alignment = Pos.CENTER_LEFT
                paddingAll = 5.0
                killbtn = button("Kill Process") {
                    hboxConstraints {
                        marginRight = 20.0
                        hGrow = Priority.ALWAYS
                    }
                }
                search = textfield() {
                    paddingTop = 5.0
                    paddingBottom = 5.0
                    paddingLeft = 10.0
                    promptText = " Search"
                    parent.requestFocus()
                    hboxConstraints {
                        marginRight = 20.0
                        hGrow = Priority.ALWAYS
                    }
                }
                SelectUser = combobox(sortProp, sort) {
                    selectionModel.selectFirst()
                }
            }
        }
        center {
            tableview = tableview() {
                placeholder = label("Loading...")
                column("PID", ProcessInfo::pidProperty).minWidth(60.0).maxWidth(90.0)
                column("Name", ProcessInfo::nameProperty).minWidth(170.0).maxWidth(400.0)
                column("User", ProcessInfo::userProperty).minWidth(90.0).maxWidth(170.0).cellFormat {
                    text = it.toString()
                    style {
                        when (it) {
                            System.getProperty("user.name") -> {
                                backgroundColor += c("#94B0EA")
                                textFill = Color.BLACK
                            }
                            "root" -> {
                                backgroundColor += c("#bbc4dd")
                                textFill = Color.BLACK
                            }
                        }
                        setAlignment(Pos.CENTER)
                    }
                }
                column("Status", ProcessInfo::statusProperty).minWidth(90.0).maxWidth(170.0).cellFormat {
                    style {
                        when (it[0]) {
                            'R' -> {
                                backgroundColor += c("#a2d87f")
                                textFill = Color.BLACK
                                text = "Running"
                            }
                            'S' -> {
                                backgroundColor += c("#7fa2d8")
                                textFill = Color.WHITE
                                text = "Sleep"
                            }
                            'I' -> {
                                backgroundColor += c("#cfd87f")
                                textFill = Color.WHITE
                                text = "Idle"
                            }
                            'D' -> {
                                backgroundColor += c("#b57fd8")
                                textFill = Color.WHITE
                                text = "Waiting"
                            }
                            'T' -> {
                                backgroundColor += c("#b8b8b8")
                                textFill = Color.BLACK
                                text = "Stopped"
                            }
                            'X' -> {
                                backgroundColor += c("#d87fa2")
                                textFill = Color.WHITE
                                text = "Dead"
                            }
                            'Z' -> {
                                backgroundColor += c("#405632")
                                textFill = Color.WHITE
                                text = "Zombie"
                            }
                        }
                        setAlignment(Pos.CENTER)
                    }
                }
                column("CPU %", ProcessInfo::loadCpuProperty).minWidth(50.0).maxWidth(90.0).cellFormat {
                    text = "%.2f".format(it)
                    style {
                        if (it.toDouble() < 70 && it.toDouble() > 40) {
                            backgroundColor += c("#EACE94")
                            textFill = Color.BLACK
                        } else if (it.toDouble() > 70) {
                            backgroundColor += c("#ff6b4c")
                            textFill = Color.WHITE
                        }
                        setAlignment(Pos.CENTER)
                    }
                }
                column("RAM %", ProcessInfo::loadRamProperty).minWidth(50.0).maxWidth(90.0).cellFormat {
                    text = "%.2f".format(it)
                    style {
                        if (it.toDouble() < 70 && it.toDouble() > 40) {
                            backgroundColor += c("#EACE94")
                            textFill = Color.BLACK
                        } else if (it.toDouble() > 70) {
                            backgroundColor += c("#ff6b4c")
                            textFill = Color.WHITE
                        }
                        setAlignment(Pos.CENTER)
                    }
                }
                column("Path", ProcessInfo::pathProperty).minWidth(100.0)
                    .contentWidth(20.0, useAsMin = true, useAsMax = false)
                contextmenu {
                    item("Kill").action {
                        if (tableview.selectionModel.selectedItem != null)
                            ProcessUtils.killProcess(tableview.selectionModel.selectedItem)
                    }
                    item("Pause").action {
                        if (tableview.selectionModel.selectedItem != null)
                            ProcessUtils.pauseProcess(tableview.selectionModel.selectedItem)
                    }
                    item("Continue").action {
                        if (tableview.selectionModel.selectedItem != null)
                            ProcessUtils.continueProcess(tableview.selectionModel.selectedItem)
                    }
                }
            }
        }
    }

    private fun sort(property: String): ObservableList<ProcessInfo> {
        return when (property) {
            "System Process" -> RefreshTable.listProcessProperty.value.filtered {
                it.user.startsWith("root")
            }
            "User Process" -> RefreshTable.listProcessProperty.value.filtered {
                it.user.startsWith(System.getProperty("user.name"))
            }
            else -> RefreshTable.listProcessProperty.value.filtered {
                it.user.startsWith("")
            }
        }
    }

    private fun refreshDataAndTAble() {
        tableview.itemsProperty().bindBidirectional(RefreshTable.listProcessProperty)
        tableview.refresh()
    }

    init {
        refreshDataAndTAble()
        search.textProperty().onChange {
            tableview.placeholder = label("Not found :(")
            if (search.text.isEmpty()) {
                RefreshTable.startRealtime()
            } else {
                RefreshTable.cancel()
                tableview.items = RefreshTable.listProcessProperty.value.filtered { it.name.startsWith(search.text) }
                tableview.refresh()
            }
        }
        SelectUser.setOnAction {
        }
        killbtn.setOnMouseClicked {
            try {
                ProcessUtils.killProcess(tableview.selectionModel.selectedItem)
            } catch (e: java.lang.IllegalStateException) {
                Notification.pushWarning(
                    "Error",
                    "Please select process"
                )
            }
        }
        tableview.onSort = object : EventHandler<SortEvent<TableView<ProcessInfo>>> {
            override fun handle(event: SortEvent<TableView<ProcessInfo>>?) {
            }
        }
    }

    private fun setHeaderClickListeners() {
        var headerRow: TableHeaderRow? = null
        for (n in (tableview.skin as TableViewSkin<*>).children) {
            if (n is TableHeaderRow) {
                headerRow = n
            }
        }
        if (headerRow == null) {
            return
        }

        val ntch = headerRow.children[1] as NestedTableColumnHeader
        val headers = ntch.columnHeaders

        for (i in headers.indices) {
            val header = headers[i]
            header.setOnMouseClicked { mouseEvent ->
                val column = header.tableColumn
                if (mouseEvent.button == MouseButton.PRIMARY && mouseEvent.clickCount == 2) {
                    println("Header cell " + i + " clicked! " + column.text)
                }
            }
        }
    }
}
