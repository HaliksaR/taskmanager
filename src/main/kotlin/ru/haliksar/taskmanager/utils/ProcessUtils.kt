package ru.haliksar.taskmanager.utils

import javafx.collections.ObservableList
import ru.haliksar.taskmanager.models.ProcessInfo
import ru.haliksar.taskmanager.utils.other.ReadCommand
import tornadofx.asObservable
import java.io.BufferedReader
import java.io.InputStreamReader

object ProcessUtils {

    fun getNumProcess(): Int {
        return ReadCommand.readFromCommand("ps -aux").size - 1
    }

    fun getInfo(): ObservableList<ProcessInfo> {
        val lineList = ReadCommand.readFromCommand("ps -aux")
        val arrayList = ArrayList<ProcessInfo>()
        for (i in 1 until lineList.size - 1)
            lineList[i].split("\\s+".toRegex()).run {
                arrayList.add(
                    ProcessInfo(
                        pid = get(1).toInt(),
                        name = getNamePid(get(1)).replace("[\\p{Ps}\\p{Pe}]".toRegex(), ""),
                        path = get(10),
                        status = get(7),
                        loadCpu = get(2).toDouble() / (CoreUtils.cores.size - 1),
                        user = get(0),
                        loadRam = get(3).toDouble()
                    )
                )
            }
        lineList.clear()
        return arrayList.asObservable()
    }

    private fun getNamePid(get: String): String {
        val p = Runtime.getRuntime().exec("ps -p $get -o comm=")
        return BufferedReader(InputStreamReader(p.inputStream)).readLines().toString()
    }

    fun killProcess(p: ProcessInfo) {
        sendSignal(p.pid.toString(), "SIGKILL")
    }

    fun pauseProcess(p: ProcessInfo) {
        sendSignal(p.pid.toString(), "SIGSTOP")
    }

    fun continueProcess(p: ProcessInfo) {
        sendSignal(p.pid.toString(), "SIGCONT")
    }

    private fun sendSignal(processId: String, signal: String) {
        Runtime.getRuntime().exec("kill -$signal $processId")
    }
}
