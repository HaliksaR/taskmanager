package ru.haliksar.taskmanager.utils.refreshers

import javafx.application.Platform
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleIntegerProperty
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.haliksar.taskmanager.models.CoreInfo
import ru.haliksar.taskmanager.models.DiskInfo
import ru.haliksar.taskmanager.models.NetworkInfo
import ru.haliksar.taskmanager.models.RamInfo
import ru.haliksar.taskmanager.models.SwapInfo
import ru.haliksar.taskmanager.utils.CoreUtils
import ru.haliksar.taskmanager.utils.DiskUtils
import ru.haliksar.taskmanager.utils.NetworkUtils
import ru.haliksar.taskmanager.utils.ProcessUtils
import ru.haliksar.taskmanager.utils.RamUtils
import ru.haliksar.taskmanager.utils.SwapUtils
import ru.haliksar.taskmanager.utils.other.Variable
import tornadofx.asObservable
import tornadofx.toObservable

class RefreshText {
    companion object : Refreshable {
        var processes: SimpleIntegerProperty = SimpleIntegerProperty()
        var usageProcessor: SimpleDoubleProperty = SimpleDoubleProperty()
        var cores: MutableMap<Int, CoreInfo> = mutableMapOf()
        var network: MutableMap<String, NetworkInfo> = mutableMapOf()
        var ram: RamInfo = RamInfo(0.0, 0.0, 0.0, 0.0)
        var swap: SwapInfo = SwapInfo(0.0, 0.0, 0.0)
        var disks: MutableMap<String, DiskInfo> = mutableMapOf()

        var sumRSpeedProperty: SimpleDoubleProperty = SimpleDoubleProperty(0.0)
        var sumTSpeedProperty: SimpleDoubleProperty = SimpleDoubleProperty(0.0)

        override var job: Job? = null

        override fun refresh(flag: Boolean) = GlobalScope.launch(Dispatchers.IO) {
            if (flag) {
                while (true) {
                    task()
                    delay(Variable.delayTextInfo)
                }
            } else {
                task()
            }
        }

        private fun task() {
            val dataNet: MutableMap<String, NetworkInfo> = NetworkUtils.getInfo()
            val dataProcesses: Int = ProcessUtils.getNumProcess()
            val dataCores: MutableMap<Int, CoreInfo> = CoreUtils.getInfo()
            val dataDisks: MutableMap<String, DiskInfo> = DiskUtils.getInfo()
            val dataRam: RamInfo = RamUtils.getInfo()
            val dataSwap: SwapInfo = SwapUtils.getInfo()
            Platform.runLater {
                processes.value = dataProcesses
                cores = dataCores
                usageProcessor.value = dataCores[0]?.usage
                network = dataNet.asObservable()
                sumRSpeedProperty.value = NetworkUtils.sumRSpeed
                sumTSpeedProperty.value = NetworkUtils.sumTSpeed
                ram.availableProperty.value = dataRam.available
                ram.usedProperty.value = dataRam.used
                ram.totalProperty.value = dataRam.total
                ram.freeProperty.value = dataRam.free
                swap.totalProperty.value = dataSwap.total
                swap.freeProperty.value = dataSwap.free
                swap.usedProperty.value = dataSwap.used
                disks = dataDisks.toObservable()
            }
        }
    }
}
