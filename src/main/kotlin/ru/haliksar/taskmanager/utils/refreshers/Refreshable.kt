package ru.haliksar.taskmanager.utils.refreshers

import kotlinx.coroutines.Job

interface Refreshable {
    var job: Job?
    fun refresh(flag: Boolean): Job

    fun startRealtime() {
        job?.cancel()
        job = refresh(true)
    }

    fun startOne() {
        job?.cancel()
        job = refresh(false)
    }

    fun cancel(): Unit? {
        return job?.cancel()
    }
}
