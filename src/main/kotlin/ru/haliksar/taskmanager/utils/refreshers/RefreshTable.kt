package ru.haliksar.taskmanager.utils.refreshers

import javafx.application.Platform
import javafx.beans.property.SimpleListProperty
import javafx.collections.FXCollections
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.haliksar.taskmanager.models.ProcessInfo
import ru.haliksar.taskmanager.utils.ProcessUtils
import ru.haliksar.taskmanager.utils.other.Variable

class RefreshTable {
    companion object : Refreshable {
        val listProcessProperty =
            SimpleListProperty<ProcessInfo>(this, "listProcess", FXCollections.observableArrayList())
            get() = field

        override var job: Job? = null

        override fun refresh(flag: Boolean) = GlobalScope.launch(Dispatchers.IO) {
            if (flag) {
                while (true) {
                    task()
                    delay(Variable.delayTable)
                }
            } else {
                task()
            }
        }

        private fun task() {
            val listThread = ProcessUtils.getInfo()
            Platform.runLater {
                listProcessProperty.value = listThread
            }
        }
    }
}
