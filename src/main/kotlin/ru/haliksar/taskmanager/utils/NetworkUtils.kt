package ru.haliksar.taskmanager.utils

import ru.haliksar.taskmanager.models.NetworkInfo
import ru.haliksar.taskmanager.utils.other.ReadCommand
import kotlin.math.absoluteValue

object NetworkUtils {
    private var interfaces: MutableMap<String, NetworkInfo> = mutableMapOf()
    var sumRSpeed: Double = 0.0
    var sumTSpeed: Double = 0.0

    fun getInfo(): MutableMap<String, NetworkInfo> {
        sumRSpeed = 0.0
        sumTSpeed = 0.0
        val data: MutableMap<String, NetworkInfo> = mutableMapOf()
        ReadCommand.readFromFile("/proc/net/dev").run {
            for (index in 2 until this.size) {
                this[index].split("\\s+".toRegex()).run {
                    var position = 0
                    if (get(0).isEmpty()) position = 1
                    val rBytes = get(position + 1).toLong()
                    val tBytes = get(position + 9).toLong()
                    data.put(
                        get(position),
                        NetworkInfo(
                            r_bytes = rBytes,
                            r_packets = get(position + 2).toLong(),
                            r_speed = calcSpeed(
                                rBytes.toDouble(),
                                (interfaces[get(position)]?.r_bytes?.toDouble() ?: 0.0)
                            ),
                            t_bytes = tBytes,
                            t_packets = get(position + 10).toLong(),
                            t_speed = calcSpeed(
                                tBytes.toDouble(),
                                (interfaces[get(position)]?.t_bytes?.toDouble() ?: 0.0)
                            )
                        )
                    )
                }
            }
        }
        interfaces = data
        interfaces.forEach {
            sumRSpeed += it.value.r_speed
            sumTSpeed += it.value.t_speed
        }
        return data
    }

    private fun calcSpeed(rBytes: Double, oldRBytes: Double): Double =
        if (oldRBytes == 0.0) 0.0 else (rBytes - oldRBytes).absoluteValue / 1000
}
