package ru.haliksar.taskmanager.utils

import ru.haliksar.taskmanager.models.CpuInfo
import ru.haliksar.taskmanager.utils.other.ReadCommand

object CpuUtils {
    fun getInfo(): CpuInfo {
        ReadCommand.readFromCommand("lscpu").run {
            return CpuInfo(
                name = mySplit(get(13)),
                architecture = mySplit(get(0)),
                vendor_id = mySplit(get(10)),
                Threads_per_core = mySplit(get(6)).toInt(),
                Cores_per_socket = mySplit(get(7)).toInt(),
                CPU_max_MHz = mySplit(get(16)).replace(",".toRegex(), ".").toDouble(),
                CPU_min_MHz = mySplit(get(17)).replace(",".toRegex(), ".").toDouble(),
                cache = arrayOf(
                    get(20).split(":\\s+|K".toRegex())[1].toDouble(),
                    get(21).split(":\\s+|K".toRegex())[1].toDouble(),
                    get(22).split(":\\s+|K".toRegex())[1].toDouble(),
                    get(23).split(":\\s+|K".toRegex())[1].toDouble()
                )
            )
        }
    }
    private fun mySplit(s: String) = s.split(":\\s+".toRegex())[1]
}
