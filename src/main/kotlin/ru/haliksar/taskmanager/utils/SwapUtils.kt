package ru.haliksar.taskmanager.utils

import ru.haliksar.taskmanager.models.SwapInfo
import ru.haliksar.taskmanager.utils.other.ReadCommand

object SwapUtils {
    fun getInfo(): SwapInfo {
        val data: SwapInfo
        ReadCommand.readFromCommand("free")[2].split("\\s+".toRegex()).run {
            data = SwapInfo(
                total = get(1).toDouble().div(1024 * 1024),
                free = get(3).toDouble().div(1024 * 1024),
                used = get(2).toDouble().div(1024 * 1024)
            )
        }
        return data
    }
}
