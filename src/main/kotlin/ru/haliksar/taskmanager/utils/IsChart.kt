package ru.haliksar.taskmanager.utils

import javafx.beans.property.SimpleListProperty
import javafx.collections.ObservableList
import javafx.scene.chart.XYChart

interface IsChart {
    var chars: SimpleListProperty<XYChart.Data<String, Number>>

    fun createCharts(): ArrayList<XYChart.Data<String, Number>>

    fun getChars(): ObservableList<XYChart.Data<String, Number>>
}
