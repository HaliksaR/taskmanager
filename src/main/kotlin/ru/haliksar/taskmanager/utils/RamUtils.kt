package ru.haliksar.taskmanager.utils

import ru.haliksar.taskmanager.models.RamInfo
import ru.haliksar.taskmanager.utils.other.ReadCommand

object RamUtils {
    fun getInfo(): RamInfo {
        var data: RamInfo
        ReadCommand.readFromCommand("free")[1].split("\\s+".toRegex()).run {
            data = RamInfo(
                total = get(1).toDouble().div(1024 * 1024),
                free = get(3).toDouble().div(1024 * 1024),
                available = get(6).toDouble().div(1024 * 1024),
                used = get(2).toDouble().div(1024 * 1024)
            )
        }
        return data
    }
}
