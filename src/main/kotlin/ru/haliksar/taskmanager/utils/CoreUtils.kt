package ru.haliksar.taskmanager.utils

import ru.haliksar.taskmanager.models.CoreInfo
import ru.haliksar.taskmanager.utils.other.ReadCommand

object CoreUtils {
    var cores: MutableMap<Int, CoreInfo> = mutableMapOf()

    fun getInfo(): MutableMap<Int, CoreInfo> {
        val data: MutableMap<Int, CoreInfo> = mutableMapOf()
        ReadCommand.readFromFile("/proc/stat").run {
            forEachIndexed { index, it ->
                if (it.startsWith("cpu")) {
                    it.split("\\s+".toRegex()).run {
                        val idle = get(4).toLong()
                        val total = get(1).toLong() + get(2).toLong() + get(3).toLong() + get(4).toLong()
                        data.put(
                            index,
                            CoreInfo(
                                total = total,
                                idle = idle,
                                usage = calculateUsage(
                                    total - (cores[index]?.total ?: 0L),
                                    idle - (cores[index]?.idle ?: 0L)
                                )
                            )
                        )
                    }
                }
            }
        }
        cores = data
        return data
    }

    private fun calculateUsage(total: Long, idle: Long) =
        if (total == 0L) 0.0 else (1000.0 * (total - idle) / total + 5.0) / 10.0 - 0.5

/*
    object Charts : IsChart {
        override var chars: SimpleListProperty<XYChart.Data<String, Number>> = SimpleListProperty()

        override fun createCharts(): ArrayList<XYChart.Data<String, Number>> {
            val data = ArrayList<XYChart.Data<String, Number>>()
            val time = SimpleDateFormat("hh:mm:ss").format(Date())
            cores.forEach {
                data.add(XYChart.Data(time, it.value.usage))
            }
            return data
        }

        override fun getChars(): ObservableList<XYChart.Data<String, Number>> {
            return chars.asObservable()
        }

    }*/
}
