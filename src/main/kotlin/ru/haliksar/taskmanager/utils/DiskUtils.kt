package ru.haliksar.taskmanager.utils

import ru.haliksar.taskmanager.models.DiskInfo
import ru.haliksar.taskmanager.utils.other.ReadCommand

object DiskUtils {
    /**
     * @param flag read disk size
     * @return {@link #data}
     * @see #getInfo()
     */
    fun getInfo(): MutableMap<String, DiskInfo> {
        val data: MutableMap<String, DiskInfo> = mutableMapOf()
        ReadCommand.readFromFile("/proc/diskstats").run {
            forEach {
                it.split("\\s+".toRegex()).run {
                    if (!get(3).startsWith("loop")) {
                        data[get(3)] = DiskInfo(
                            name = get(3),
                            blocks = 0L,
                            r_completed = get(4).toLong(),
                            r_merged = get(5).toLong(),
                            r_sectors = get(6).toLong(),
                            r_time_spent = get(7).toLong(),
                            w_completed = get(8).toLong(),
                            w_merged = get(9).toLong(),
                            w_sectors = get(10).toLong(),
                            w_time_spent = get(11).toLong(),
                            IO_currently = get(12).toLong(),
                            IO_time_spent = get(13).toLong(),
                            IO_weighted_time_spent = get(14).toLong()
                        )
                    }
                }
            }
        }
        ReadCommand.readFromFile("/proc/partitions").run {
            for (i in 2 until this.size) {
                this[i].split("\\s+".toRegex()).run {
                    if (data.keys.contains(get(4)))
                        data[get(4)]!!.blocks = get(3).toLong()
                }
            }
        }
/*            data.forEach {
                println(it.key + " " + it.value.toString())
            }*/
        return data
    }
}
