package ru.haliksar.taskmanager.utils.other

import org.controlsfx.control.Notifications

object Notification {
    fun pushWarning(title: String, massage: String) {
        Notifications.create().title(title).text(massage).showWarning()
    }
}
