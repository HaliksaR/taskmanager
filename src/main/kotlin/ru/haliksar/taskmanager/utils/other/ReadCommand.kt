package ru.haliksar.taskmanager.utils.other

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.nio.file.Files
import java.nio.file.Paths

object ReadCommand {
    fun readFromCommand(command: String): MutableList<String> {
        val lineList = mutableListOf<String>()
        val input = BufferedReader(InputStreamReader(Runtime.getRuntime().exec(command).inputStream))
        input.useLines { lines -> lines.forEach { lineList.add(it) } }
        input.close()
        return lineList
    }
    fun readFromFile(path: String): MutableList<String> {
        try {
            val lineList = mutableListOf<String>()
            Files.lines(Paths.get(path)).forEach {
                    lineList.add(it)
            }
            return lineList
/*            BufferedReader(InputStreamReader(FileInputStream(path))).use { cpuReader ->
                val lineList = mutableListOf<String>()
                cpuReader.useLines { lines -> lines.forEach { lineList.add(it) } }
                return lineList
            }*/
        } catch (e: IOException) {
        } catch (e: NumberFormatException) {
        }
        return MutableList(0) { "" }
    }
}
