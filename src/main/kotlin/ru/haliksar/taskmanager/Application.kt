package ru.haliksar.taskmanager

import javafx.stage.Stage
import ru.haliksar.taskmanager.utils.refreshers.RefreshTable
import ru.haliksar.taskmanager.utils.refreshers.RefreshText
import ru.haliksar.taskmanager.view.HomeView
import tornadofx.App

// --module-path /usr/lib/jvm/javafx-sdk-11.0.2/lib/ --add-modules=javafx.controls

class Application : App(HomeView::class) {
    override val primaryView = HomeView::class

    override fun start(stage: Stage) {
        with(stage) {
            minWidth = 600.0
            minHeight = 550.0
        }
        super.start(stage)
    }

    override fun stop() {
        super.stop()
        RefreshTable.cancel()
        RefreshText.cancel()
    }
}
