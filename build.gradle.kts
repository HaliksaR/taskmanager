import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    kotlin("jvm") version "1.3.50"
    id("org.openjfx.javafxplugin") version "0.0.8"
    java
}

group = "ru.curs-os"
version = "1.0-SNAPSHOT"


application {
    mainClassName = "ru.haliksar.taskmanager.Application"
    applicationDefaultJvmArgs = listOf(
        "-XX:+UnlockCommercialFeatures", "-XX:+FlightRecorder",
        "--add-opens=javafx.base/com.sun.javafx.runtime=ALL-UNNAMED",
        "--add-opens=javafx.base/com.sun.javafx.collections=ALL-UNNAMED",
        "--add-opens=javafx.graphics/com.sun.javafx.css=ALL-UNNAMED",
        "--add-opens=javafx.graphics/com.sun.javafx.scene=ALL-UNNAMED",
        "--add-opens=javafx.graphics/com.sun.javafx.scene.traversal=ALL-UNNAMED",
        "--add-opens=javafx.graphics/javafx.scene=ALL-UNNAMED",
        "--add-opens=javafx.controls/com.sun.javafx.scene.control=ALL-UNNAMED",
        "--add-opens=javafx.controls/com.sun.javafx.scene.control.behavior=ALL-UNNAMED",
        "--add-opens=javafx.controls/javafx.scene.control.skin=ALL-UNNAMED"
    )
}
javafx {
    modules = listOf(
        "javafx.base",
        "javafx.graphics",
        "javafx.controls"
    )
    version = "13"
}


repositories {
    mavenCentral()
    jcenter()
    google()
    maven {
        url = uri("https://jitpack.io")
    }
}

val ktlint by configurations.creating

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    compile(kotlin("test"))
    compile(kotlin("test-junit"))

    // JUnit 5
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.2.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.2.0")
    testRuntime("org.junit.platform:junit-platform-console:1.2.0")

    // Kotlintest
    testCompile("io.kotlintest:kotlintest-core:3.1.0-RC2")
    testCompile("io.kotlintest:kotlintest-assertions:3.1.0-RC2")
    testCompile("io.kotlintest:kotlintest-runner-junit5:3.1.0-RC2")


    implementation("no.tornado:tornadofx:1.7.19")
    implementation("org.controlsfx:controlsfx:11.0.0")

    implementation("org.openjfx:javafx-controls:13")
    implementation("org.openjfx:javafx-base:13")
    implementation("org.openjfx:javafx-graphics:13")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.2")

    ktlint("com.pinterest:ktlint:0.35.0")
}


sourceSets {
    main {
        output.setResourcesDir(File("build/classes/kotlin/main"))
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
    sourceCompatibility = "13"
    targetCompatibility = "1.8"
}

tasks.withType<JavaCompile> {
    sourceCompatibility = "13"
    targetCompatibility = "1.8"
}

val ktlintCheck = tasks.register<JavaExec>("ktlint") {
    group = LifecycleBasePlugin.VERIFICATION_GROUP
    description = "Check Kotlin code style"
    classpath = ktlint
    main = "com.pinterest.ktlint.Main"
    args("src/**/*.kt")
}

val ktlintFormat = tasks.register<JavaExec>("ktlintFormat") {
    group = LifecycleBasePlugin.VERIFICATION_GROUP
    description = "Fix Kotlin code style deviations"
    classpath = ktlint
    main = "com.pinterest.ktlint.Main"
    args("-F", "src/**/*.kt")
}

val fatJar = task("fatJar", type = Jar::class) {
    baseName = "${project.name}-fat"
    manifest {
        attributes["Implementation-Title"] = "Gradle Jar File Example"
        attributes["Implementation-Version"] = version
        attributes["Main-Class"] = application.mainClassName
    }
    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
    with(tasks.jar.get() as CopySpec)
}

tasks {
    "test"(Test::class) {
        useJUnitPlatform()
    }
    "build" {
        dependsOn(fatJar)
    }
    register("build-Jar")
    "build-Jar" {
        dependsOn(fatJar)
    }
    register("check-style")
    "check-style" {
        dependsOn(ktlintCheck)
    }
    register("formatting")
    "formatting" {
        dependsOn(ktlintFormat)
    }
}